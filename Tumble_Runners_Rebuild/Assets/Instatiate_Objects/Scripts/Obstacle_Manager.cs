﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Utility_Collection;
using Random = UnityEngine.Random;


public class Obstacle_Manager : MonoBehaviour
{

    public float spawn_Delay;
    public float audio_Time;
    public float object_Life_Time;

    private Transform m_Instance_Hold_Transform;
    private Transform m_Spawn_Transform;
    private float m_Spawn_Timer;
    private float m_Audio_Timer;

    private List<Transform> m_Lighter_Instance;
    private List<Transform> m_Pushpin_Instance;
    private List<Transform> m_Safty_Needle_Instance;
    private List<Transform> m_Telephone_Instance;
    private List<Transform> m_Tennis_Ball_Instance;


    private void Awake ()
    {
        m_Instance_Hold_Transform = GameObject.FindGameObjectWithTag("Obstacle_Holder").transform;
        m_Spawn_Transform = GameObject.FindGameObjectWithTag("Obstacle_Spawn_Point").transform;

        m_Lighter_Instance = new List<Transform>();
        m_Pushpin_Instance = new List<Transform>();
        m_Safty_Needle_Instance = new List<Transform>();
        m_Telephone_Instance = new List<Transform>();
        m_Tennis_Ball_Instance = new List<Transform>();
    }

    
    private void Start ()
    {
        for (int i = 0; i < 5; i++)
        {
            Add_Lighter_Object();
            Add_Pushpin_Object();
            Add_Safty_Needle_Object();
            Add_Telephone_Object();
            Add_Tennis_Ball_Object();
        }

        m_Spawn_Timer = spawn_Delay - audio_Time;
        m_Audio_Timer = audio_Time;
    }

    
    private void Update ()
    {
        if (m_Spawn_Timer > 0.0f)
        {
            m_Spawn_Timer -= Time.deltaTime;
            if (m_Spawn_Timer <= 0.0f)
            {
                m_Spawn_Timer = 0.0f;
                // Play audio
            }
        }
        else if (m_Audio_Timer > 0.0f && m_Spawn_Timer == 0.0f)
        {
            m_Audio_Timer -= Time.deltaTime;
            if (m_Audio_Timer <= 0.0f)
                Drop_Obstacle();
        }
    }


    private void Drop_Obstacle()
    {
        int spawn_Idex = Random.Range(1, 5);
        switch (spawn_Idex)
        {
            case 1:
                Initialize_Object(m_Lighter_Instance);
                break;
            case 2:
                Initialize_Object(m_Lighter_Instance);
                break;
            case 3:
                Initialize_Object(m_Lighter_Instance);
                break;
            case 4:
                Initialize_Object(m_Lighter_Instance);
                break;
            case 5:
                Initialize_Object(m_Lighter_Instance);
                break;
        }
        m_Spawn_Timer = spawn_Delay - audio_Time;
        m_Audio_Timer = audio_Time;
    }


    private void Initialize_Object(List<Transform> object_List)
    {
        Transform instance = object_List[0];
        object_List.Remove(instance);
        instance.SetParent(null);
        instance.position = m_Spawn_Transform.position;
        instance.GetComponent<Obstacle_Controller>().Activate_Object(object_Life_Time);
    }


    private void Add_Lighter_Object ()
    {
        Transform obstacle_Transform = Instantiate(Resources.Load("Resources/Obstacle/Obstacle_Lighter", typeof(Transform))) as Transform;
        obstacle_Transform.GetComponent<Obstacle_Controller>().Intialize(this);
        obstacle_Transform.SetParent(m_Instance_Hold_Transform);
        obstacle_Transform.localPosition = Vector3.zero;
        m_Lighter_Instance.Add(obstacle_Transform);
    }


    private void Add_Pushpin_Object()
    {
        Transform obstacle_Transform = Instantiate(Resources.Load("Resources/Obstacle/Obstacle_Pushpin", typeof(Transform))) as Transform;
        obstacle_Transform.GetComponent<Obstacle_Controller>().Intialize(this);
        obstacle_Transform.SetParent(m_Instance_Hold_Transform);
        obstacle_Transform.localPosition = Vector3.zero;
        m_Pushpin_Instance.Add(obstacle_Transform);
    }


    private void Add_Safty_Needle_Object()
    {
        Transform obstacle_Transform = Instantiate(Resources.Load("Resources/Obstacle/Obstacle_Safty_Needle", typeof(Transform))) as Transform;
        obstacle_Transform.GetComponent<Obstacle_Controller>().Intialize(this);
        obstacle_Transform.SetParent(m_Instance_Hold_Transform);
        obstacle_Transform.localPosition = Vector3.zero;
        m_Safty_Needle_Instance.Add(obstacle_Transform);
    }


    private void Add_Telephone_Object()
    {
        Transform obstacle_Transform = Instantiate(Resources.Load("Resources/Obstacle/Obstacle_Telephone", typeof(Transform))) as Transform;
        obstacle_Transform.GetComponent<Obstacle_Controller>().Intialize(this);
        obstacle_Transform.SetParent(m_Instance_Hold_Transform);
        obstacle_Transform.localPosition = Vector3.zero;
        m_Telephone_Instance.Add(obstacle_Transform);
    }


    private void Add_Tennis_Ball_Object()
    {
        Transform obstacle_Transform = Instantiate(Resources.Load("Resources/Obstacle/Obstacle_Tennis_Ball", typeof(Transform))) as Transform;
        obstacle_Transform.GetComponent<Obstacle_Controller>().Intialize(this);
        obstacle_Transform.SetParent(m_Instance_Hold_Transform);
        obstacle_Transform.localPosition = Vector3.zero;
        m_Tennis_Ball_Instance.Add(obstacle_Transform);
    }


    public void Remove_Object (Transform object_Transform, Obstacle_Type obstacle_Type)
    {
        object_Transform.SetParent(m_Instance_Hold_Transform);
        object_Transform.localPosition = Vector2.zero;

        switch (obstacle_Type)
        {
            case Obstacle_Type.Lighter:
                m_Lighter_Instance.Add(object_Transform);
                break;
            case Obstacle_Type.Pushpin:
                m_Pushpin_Instance.Add(object_Transform);
                break;
            case Obstacle_Type.Safty_Needle:
                m_Safty_Needle_Instance.Add(object_Transform);
                break;
            case Obstacle_Type.Telephone:
                m_Telephone_Instance.Add(object_Transform);
                break;
            case Obstacle_Type.Tennis_Ball:
                m_Tennis_Ball_Instance.Add(object_Transform);
                break;
        }
    }


    public void On_Direction_Change()
    {
        Vector3 curr_Pos = m_Spawn_Transform.position;
        curr_Pos.x = -curr_Pos.x;
        m_Spawn_Transform.position = curr_Pos;
    }


}



