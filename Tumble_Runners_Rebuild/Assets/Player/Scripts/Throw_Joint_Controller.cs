﻿using UnityEngine;
using System.Collections;

public class Throw_Joint_Controller : MonoBehaviour
{

    private Transform m_Player_Transform;


    public void Intialize_Joint_Object (Transform player_Transform)
    {
        m_Player_Transform = player_Transform;
    }


    private void OnJointBreak2D(Joint2D brokenJoint)
    {
        m_Player_Transform.BroadcastMessage("On_Throw_Spring_Break", SendMessageOptions.RequireReceiver);
        Debug.Log("Joint break force: " + brokenJoint.reactionForce.magnitude);
    }


}
