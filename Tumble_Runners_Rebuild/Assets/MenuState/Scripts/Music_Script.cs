﻿using UnityEngine;
using System.Collections;

public class Music_Script : MonoBehaviour
{
    public static Music_Script m_instance;
    public AudioSource m_Source;
    public AudioClip m_Clip_One;
    public AudioClip m_Clip_Two;
    public bool m_fade;
    public float m_fadeSpeed;
    private bool m_Played = false;
    private float m_volume = 1;
	
    void Start()
    {
        m_instance = this;
    }

	// Update is called once per frame
	void Update ()
    {
        if(!m_Played)
        {
            m_Source.PlayOneShot(m_Clip_One);
            m_Played = true;
        }
        else if(!m_Source.isPlaying)
        {
            m_Source.PlayOneShot(m_Clip_Two);
        }

        if(m_fade)
        {
            m_Source.volume = m_volume;
            m_volume -= m_fadeSpeed;
        }
	}
}
