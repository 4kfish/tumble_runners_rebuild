﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Utility_Collection;


public class Pickup_Manager : MonoBehaviour
{

    public int instances_Per_Player;
    public float coin_Value;
    public float ring_Value;
    
    private Transform m_Transform;
    private Transform m_Instance_Hold_Transform;
    private Transform m_Spawn_Transform;
    private Player_Manager m_Player_Manager;
    private List<Transform> m_Coin_Instance;
    private List<Transform> m_Ring_Instance;


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Instance_Hold_Transform = GameObject.FindGameObjectWithTag("Pickup_Holder").transform;
        m_Spawn_Transform = GameObject.FindGameObjectWithTag("Pickup_Spawn_Point").transform;
        m_Player_Manager = m_Transform.GetComponent<Player_Manager>();

        m_Coin_Instance = new List<Transform>();
        m_Ring_Instance = new List<Transform>();
    }


    private void Start ()
    {
        int amount = instances_Per_Player * m_Player_Manager.Active_Player_Amount;

        for (int i = 0; i < amount; i++)
        {
            Transform coin_Transform = Instantiate(Resources.Load("Resources/Pickups/Pickup_Coin", typeof(Transform))) as Transform;
            coin_Transform.GetComponent<Pickup_Controller>().Intialize(this, m_Player_Manager, coin_Value);
            coin_Transform.SetParent(m_Instance_Hold_Transform);
            coin_Transform.localPosition = Vector3.zero;
            m_Coin_Instance.Add(coin_Transform);

            Transform ring_Transform = Instantiate(Resources.Load("Resources/Pickups/Pickup_Ring", typeof(Transform))) as Transform;
            ring_Transform.GetComponent<Pickup_Controller>().Intialize(this, m_Player_Manager, coin_Value);
            ring_Transform.SetParent(m_Instance_Hold_Transform);
            ring_Transform.localPosition = Vector3.zero;
            m_Ring_Instance.Add(ring_Transform);
        }
    }

    
    private void Update ()
    {

    }


    public void Remove_Object(Transform object_Transform, Pickup_Type pickup_Type)
    {
        object_Transform.SetParent(m_Instance_Hold_Transform);
        object_Transform.localPosition = Vector2.zero;

        if (pickup_Type == Pickup_Type.Coin)
            m_Coin_Instance.Add(object_Transform);
        else if (pickup_Type == Pickup_Type.Ring)
            m_Ring_Instance.Add(object_Transform);
    }


    public void On_Direction_Change ()
    {
        Vector3 curr_Pos = m_Spawn_Transform.position;
        curr_Pos.x = -curr_Pos.x;
        m_Spawn_Transform.position = curr_Pos;
    }


}



