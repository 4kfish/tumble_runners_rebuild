﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(HingeJoint2D))]
[RequireComponent(typeof(LineRenderer))]


public class Ragdoll_Controller2D : MonoBehaviour
{

    public float ragdoll_Length;
    public float ragdoll_Width;
    public int ragdoll_Segments;
    public Start_Direction start_Direction;

    public bool limit_Ragdoll_Rotation;
    public bool limit_Main_Joint_Rotation;
    public float min_Rotation;
    public float max_Rotation;
    public bool limit_Ragdoll_Stretch;
    [Range(0.01f, 0.2f)]
    public float max_Stretch_Percent;

    public float body_Mass;
    public Weight_Curve weight_Curve;
    public float body_Gravity_Scale;

    public LayerMask layer_Mask;
    public Rigidbody2D main_Connected_Body;
    public Vector2 main_Body_Offset;
    public Rigidbody2D end_Connected_Body;


    public string sorting_Layer;
    public int order_In_Layer;
    public Material material;
    public Color color;

    private Transform[] m_Transforms;
    private LineRenderer m_Line_Renderer;
    private Rigidbody2D[] m_Rigidbodys;
    private HingeJoint2D[] m_Hinge_Joints;
    private BoxCollider2D[] m_Box_Colliders;
    private HingeJoint2D m_External_Connected_Joint;
    private bool m_External_Joint_Distance_Break;


    public enum Weight_Curve
    {
        Linear,
        Linear_Divided,
        Non_Linear,
        Non_Linear_Divided
    }

	public enum Start_Direction
    {
        Left,
        Right,
        Up,
        Down
    }


    private void Awake ()
    {
        m_Transforms = new Transform[ragdoll_Segments + 1];
        m_Transforms[0] = GetComponent<Transform>();

        m_Rigidbodys = new Rigidbody2D[ragdoll_Segments + 1];
        m_Rigidbodys[0] = m_Transforms[0].GetComponent<Rigidbody2D>();
        
        m_Hinge_Joints = new HingeJoint2D[ragdoll_Segments + 1];
        m_Hinge_Joints[0] = m_Transforms[0].GetComponent<HingeJoint2D>();
        
        m_Box_Colliders = new BoxCollider2D[ragdoll_Segments + 1];
        m_Box_Colliders[0] = m_Transforms[0].GetComponent<BoxCollider2D>();
        
        m_Line_Renderer = m_Transforms[0].GetComponent<LineRenderer>();
    }


	private void Start ()
    {
        m_Line_Renderer.SetVertexCount(ragdoll_Segments + 1);
        m_Line_Renderer.sortingLayerName = sorting_Layer;
        m_Line_Renderer.sortingOrder = order_In_Layer;
        m_Line_Renderer.material = material;
        m_Line_Renderer.SetColors(color, color);
        m_Line_Renderer.SetWidth(ragdoll_Width, ragdoll_Width);

        Initialize_Root_Joint();
        Intitalize_Segments();
        Update_Line_Renderer();
    }


    private void FixedUpdate ()
    {
        Update_Line_Renderer();

        if (limit_Ragdoll_Stretch) {
            Limit_Ragdoll_Stretch();
        }

    }


    private void Update_Line_Renderer ()
    {
        for (int i = 0; i < m_Transforms.Length; i++)
        {
            m_Line_Renderer.SetPosition(i, m_Transforms[i].position);
        }
    }


    private void Limit_Ragdoll_Stretch ()
    {
        float delta_Max_Dist = (ragdoll_Length / ragdoll_Segments); // * (1.0f + max_Stretch_Percent);

        for (int i = 1; i < m_Transforms.Length; i++)
        {
            if (Vector2.Distance(m_Transforms[i].position, m_Transforms[i - 1].position) > delta_Max_Dist)
            {
                Vector3 normal = (m_Transforms[i].position - m_Transforms[i - 1].position).normalized;
                m_Transforms[i].position = m_Transforms[i - 1].position + (normal * delta_Max_Dist);
            }

        }
    }


    private void Initialize_Root_Joint ()
    {
        float delta_Distance = ragdoll_Length / ragdoll_Segments;

        m_Transforms[0].gameObject.layer = layer_Mask;
        m_Box_Colliders[0].size = set_Box_Collider_Size(delta_Distance, ragdoll_Width, start_Direction);
        m_Box_Colliders[0].offset = set_Box_Collider_Offset(delta_Distance, start_Direction);

        if (limit_Main_Joint_Rotation)
        {
            m_Hinge_Joints[0].useLimits = true;
            JointAngleLimits2D limits = new JointAngleLimits2D();
            limits.min = min_Rotation;
            limits.max = max_Rotation;
            m_Hinge_Joints[0].limits = limits;
        }
        m_Hinge_Joints[0].autoConfigureConnectedAnchor = false;
        m_Hinge_Joints[0].connectedBody = main_Connected_Body;
        m_Hinge_Joints[0].connectedAnchor = main_Body_Offset;
    }


    private void Intitalize_Segments ()
    {
        float delta_Distance = ragdoll_Length / ragdoll_Segments;

        Vector2 direction = Vector2.right;

        if (start_Direction == Start_Direction.Left)
            direction = Vector2.left;
        if (start_Direction == Start_Direction.Up)
            direction = Vector2.up;
        if (start_Direction == Start_Direction.Down)
            direction = Vector2.down;

        for (int i = 1; i <= ragdoll_Segments; i++)
        {
            GameObject segment = new GameObject();
            segment.name = "Segment_" + i.ToString();
            segment.layer = m_Transforms[i -1].gameObject.layer;

            Vector2 position = direction * delta_Distance;
            m_Transforms[i] = segment.transform;
            m_Transforms[i].SetParent(m_Transforms[0]);
            m_Transforms[i].localPosition = m_Transforms[i - 1].position + (Vector3)position;

            m_Box_Colliders[i] = segment.AddComponent<BoxCollider2D>();
            m_Box_Colliders[i].size = set_Box_Collider_Size(delta_Distance, ragdoll_Width, start_Direction);
            m_Box_Colliders[i].offset = set_Box_Collider_Offset(delta_Distance, start_Direction);

            m_Rigidbodys[i] = segment.AddComponent<Rigidbody2D>();
            m_Rigidbodys[i].gravityScale = body_Gravity_Scale;
            m_Rigidbodys[i].mass = set_Body_Mass(body_Mass, i, weight_Curve);

            Debug.Log(position);
            m_Hinge_Joints[i] = segment.AddComponent<HingeJoint2D>();
            m_Hinge_Joints[i].connectedBody = m_Rigidbodys[i - 1];
            m_Hinge_Joints[i].autoConfigureConnectedAnchor = false;
            m_Hinge_Joints[i].connectedAnchor = position;
            if (limit_Ragdoll_Rotation)
            {
                m_Hinge_Joints[i].useLimits = true;
                JointAngleLimits2D limits = new JointAngleLimits2D();
                limits.min = min_Rotation;
                limits.max = max_Rotation;
                m_Hinge_Joints[i].limits = limits;
            }

        }
    }


    private float set_Body_Mass (float mass, int index, Weight_Curve weight_Curve)
    {
        var value = 0.0f;
        
        switch (weight_Curve)
        {
            case Weight_Curve.Linear:
                value = mass;
                break;
            case Weight_Curve.Linear_Divided:
                value = mass / ragdoll_Segments;
                break;
            case Weight_Curve.Non_Linear:
                value = mass * (index + 1);
                break;
            case Weight_Curve.Non_Linear_Divided:
                value = (mass / Arithmetic_Sum(1, ragdoll_Segments)) * (index + 1);
                break;
        }

        return value;
    }


    private Vector2 set_Box_Collider_Size (float length, float height, Start_Direction start_Direction)
    {
        var size = Vector2.zero;

        if (start_Direction == Start_Direction.Left || start_Direction == Start_Direction.Right)
        {
            size = new Vector2(length, height);
        }
        else if (start_Direction == Start_Direction.Up || start_Direction == Start_Direction.Down)
        {
            size = new Vector2(height, length);
        }

        return size;
    }


    private Vector2 set_Box_Collider_Offset (float length, Start_Direction start_Direction)
    {
        var offset = Vector2.zero;

        switch (start_Direction)
        {
            case Start_Direction.Left: offset.x = -length * 0.5f;
                break;
            case Start_Direction.Right: offset.x = length * 0.5f;
                break;
            case Start_Direction.Up: offset.y = length * 0.5f;
                break;
            case Start_Direction.Down: offset.y = -length * 0.5f;
                break;
        }

        return offset;
    }


    private static float Arithmetic_Sum(int Start_Value, int End_Value)
    {
        return (End_Value * (Start_Value + End_Value)) / 2;
    }


    public void Set_Connected_Anchor_Last (HingeJoint2D connected_Joint, bool joint_Distance_Break)
    {
        int last_Index = m_Hinge_Joints.Length - 1;
        m_External_Connected_Joint = connected_Joint;
        m_External_Connected_Joint.connectedBody = m_Rigidbodys[last_Index];
    }


    public void Remove_Connected_Anchor_Last ()
    {
        m_External_Connected_Joint.connectedBody = null;
        m_External_Connected_Joint = null;
    }


}



