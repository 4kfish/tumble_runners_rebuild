﻿using UnityEngine;
using System.Collections;
using Utility_Collection;


public class Obstacle_Controller : MonoBehaviour
{

    public Obstacle_Type obstacle_Type;

    private Transform m_Transform;
    private Rigidbody2D m_Rigidbody;
    private Collider2D m_Collider;
    private Obstacle_Manager m_Obstacle_Manager;
    private float m_Life_Timer;


    public void Intialize(Obstacle_Manager obstacle_Manager)
    {
        m_Transform = GetComponent<Transform>();
        m_Rigidbody = m_Transform.GetComponent<Rigidbody2D>();
        m_Rigidbody.isKinematic = true;
        m_Collider = m_Transform.GetComponent<Collider2D>();
        m_Collider.enabled = false;
        m_Obstacle_Manager = obstacle_Manager;
    }


    public void Activate_Object(float life_Time)
    {
        m_Rigidbody.isKinematic = false;
        m_Collider.enabled = true;
        m_Life_Timer = life_Time;
    }


    private void Update ()
    {
        if (m_Life_Timer > 0.0f)
        {
            m_Life_Timer -= Time.deltaTime;

            if (m_Life_Timer <= 0.0f)
            {
                m_Rigidbody.isKinematic = true;
                m_Collider.enabled = false;
                m_Obstacle_Manager.Remove_Object(m_Transform, obstacle_Type);
            }
        }
    }
}
