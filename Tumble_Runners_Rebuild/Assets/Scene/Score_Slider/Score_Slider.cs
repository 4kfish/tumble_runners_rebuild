﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Player_Collection;


public class Score_Slider : MonoBehaviour
{
    public Player_ID player_ID_Reference;
    public Color set_Slider_Overlay_Color {
        set {
            m_Change_Color = true;
            m_Slider_Overlay_Color = value;
        }
    }
    public float get_Fill_Amount { get { return m_Slider_Image.fillAmount; } }

    private Color m_Slider_Overlay_Color;
    private Image m_Slider_Image;
    private Image m_Overlay_Image;
    private bool m_Change_Color;
    private bool m_Coin_Animation_Start;
    private float m_Coin_Timer;
    private GameObject m_Dirt_1;
    private GameObject m_Dirt_2;
    private GameObject m_Dirt_3;
    private GameObject m_Clean;
    private GameObject m_Coin_Feedback;


    private void Awake ()
    {
        m_Slider_Image = transform.FindChild("Slider").GetComponent<Image>();
        m_Overlay_Image = transform.FindChild("Overlay").GetComponent<Image>();
    }


    private void Start ()
    {
        m_Slider_Image.fillAmount = 0.0f;
        m_Overlay_Image.color = m_Slider_Overlay_Color;

        m_Dirt_1 = transform.FindChild("Overlay_Dirt_Level1").gameObject;
        m_Dirt_2 = transform.FindChild("Overlay_Dirt_Level2").gameObject;
        m_Dirt_3 = transform.FindChild("Overlay_Dirt_Level3").gameObject;
        m_Clean = transform.FindChild("Overlay_Clean").gameObject;

        m_Coin_Feedback = transform.FindChild("Coin_Feedback").gameObject;
    }
	

    private void Update ()
    {
        if (m_Change_Color)
        {
            m_Overlay_Image.color = m_Slider_Overlay_Color;
            m_Change_Color = false;
        }
        
        if (m_Slider_Image.fillAmount > 0.2f && m_Slider_Image.fillAmount < 0.4f)
        {
            m_Dirt_1.SetActive(false);
            m_Dirt_2.SetActive(true);
            Debug.Log("bruh");
        }
        else if (m_Slider_Image.fillAmount > 0.4f && m_Slider_Image.fillAmount < 0.6f)
        {
            m_Dirt_2.SetActive(false);
            m_Dirt_3.SetActive(true);
            Debug.Log("breh");
        }
        else if (m_Slider_Image.fillAmount > 0.6f && m_Slider_Image.fillAmount < 0.8f)
        {
            m_Dirt_3.SetActive(false);
        }
        else if (m_Slider_Image.fillAmount > 0.8f)
        {
            m_Clean.SetActive(true);
        }

        if(m_Coin_Animation_Start)
        {
            m_Coin_Feedback.SetActive(true);

            m_Coin_Timer += Time.deltaTime;

            if(m_Coin_Timer > 0.389f)
            {
                m_Coin_Feedback.SetActive(false);
                m_Coin_Animation_Start = false;
                m_Coin_Timer = 0.0f;
            }
        }
        else
        {
            m_Coin_Feedback.SetActive(false);
        }


    }

	
	public void On_Value_Change (float new_Value)
    {
        m_Slider_Image.fillAmount = new_Value;
    }


    public void On_Pickup_Present()
    {
        m_Coin_Animation_Start = true;
    }
}



