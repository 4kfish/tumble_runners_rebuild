﻿using UnityEngine;
using System.Collections;

public class COIN_TEST : MonoBehaviour {
    
    private Player_Manager m_Player_Manager;
    

	void Start ()
    {
        m_Player_Manager = GameObject.Find("Scene").GetComponent<Player_Manager>();
	}
	

	void Update ()
    {
	    
	}  

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.transform.tag == "Player")
        {
            m_Player_Manager.On_Object_Pickup(col.transform, 0.2f);
        }

    }
}
