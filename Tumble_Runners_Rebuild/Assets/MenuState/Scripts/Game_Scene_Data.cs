﻿using UnityEngine;
using System.Collections;

public class Game_Scene_Data : MonoBehaviour
{

    public bool[] activated_Players;

    private void Awake ()
    {
        activated_Players = new bool[4];
        DontDestroyOnLoad(this);
    }

}
