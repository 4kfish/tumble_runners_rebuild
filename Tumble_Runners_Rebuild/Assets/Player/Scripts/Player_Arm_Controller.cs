﻿using UnityEngine;
using System;
using System.Collections;
using Utility_Collection;
using Player_Collection;

//[RequireComponent(typeof(Ragdoll_Controller2D))]


public class Player_Arm_Controller : Player_Controller
{

    public float max_Distance;
    public float min_Distance;
    public LayerMask layer_Mask;
    public Vector2 throw_Offset;
    public Vector2 grab_Offset;
    public float throw_Frequency;
    public float throw_Vertical_Force;
    public float break_Force;
    [Range(0.1f, 0.9f)]
    public float spring_Delta_Distance = 0.1f;

    private Transform m_Transform;
    private Transform m_Root_Transform;
    private Transform m_Throw_Transform;
    //private Ragdoll_Controller2D m_Ragdoll_Controller; implement later
    private Player_Movement_Controller m_Target_Movement_Controller;
    private Player_ID m_Player_ID;
    public bool m_Throwing;
    

    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Root_Transform = m_Transform.root;
//        m_Ragdoll_Controller = m_Transform.GetComponent<Ragdoll_Controller2D>();
    }


	private void Start ()
    {
        
    }
	

	private void Update ()
    {
        if (get_Interact_Button(m_Player_ID, Button_Event_State.Down) && !m_Throwing)
        {
            RaycastHit2D hit_Info = Physics2D.Raycast(m_Transform.position, m_Root_Transform.right, max_Distance);
            
            if (hit_Info.collider != null && hit_Info.distance > min_Distance)
            {
                m_Throw_Transform = Initialize_Trow_Joint(hit_Info.rigidbody);
            }
        }
	}


    private Transform Initialize_Trow_Joint (Rigidbody2D connected_Rigidbody)
    {
        GameObject joint_Holder = new GameObject();
        Vector3 offset = (Vector3.up * throw_Offset.y) + (m_Root_Transform.right * throw_Offset.x);
        joint_Holder.transform.position = m_Root_Transform.position + offset;

        Rigidbody2D body = joint_Holder.AddComponent<Rigidbody2D>();
        body.isKinematic = true;

        SpringJoint2D joint = joint_Holder.AddComponent<SpringJoint2D>();
        joint.autoConfigureConnectedAnchor = false;
        joint.autoConfigureDistance = false;
        joint.connectedAnchor = grab_Offset;
        joint.distance = Vector2.Distance
        (
            joint_Holder.transform.position,
            connected_Rigidbody.transform.position + (Vector3)grab_Offset
        ) * spring_Delta_Distance;

        joint.frequency = throw_Frequency;
        joint.breakForce = break_Force;
        joint.connectedBody = connected_Rigidbody;
        Throw_Joint_Controller joint_Controller = joint_Holder.AddComponent<Throw_Joint_Controller>();
        joint_Controller.Intialize_Joint_Object(m_Root_Transform);

        connected_Rigidbody.GetComponent<Player_Movement_Controller>().On_Player_Throw(throw_Vertical_Force);
        m_Throwing = true;

        return joint_Holder.GetComponent<Transform>();
    }


    public void On_Throw_Spring_Break ()
    {
        Destroy(m_Throw_Transform.gameObject);
        m_Throw_Transform = null;
        m_Throwing = false;
    }


    public void Initialize_Player_ID(Player_ID player_ID)
    {
        m_Player_ID = player_ID;
    }


}



