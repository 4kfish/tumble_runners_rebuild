﻿using UnityEngine;
using System;
using System.Collections;
using Utility_Collection;
using Player_Collection;


public class Player_Manager : Player_Controller
{
    public float button_Hit_Fill_Amount;
    public float target_Water_Time;
    public Color[] slider_Overlay_Color;

    private bool[] m_Players_In_Game;
    private Transform[] m_Spawn_Transforms;
    public Transform[] m_Player_Transforms;
    private Transform m_Lead_Ref_Point;
    public Score_Slider[] m_Score_Sliders;
    private float[] m_Slider_Values;
    private Transform m_Transform;
    private Game_Controller m_Game_Controller;
    private int m_Active_Player_Amounts;
    private bool m_Player_Spawned_In, m_Game_Over;

    public int Active_Player_Amount { get { return m_Active_Player_Amounts; } }
    public Transform get_Leading_Player
    {
        get
        {
            float value = 0.0f;
            int index = 0;

            for (int i = 0; i < m_Player_Transforms.Length; i++)
            {
                if (m_Player_Transforms[i] != null)
                {
                    if (m_Score_Sliders[i].get_Fill_Amount > value)
                    {
                        value = m_Score_Sliders[i].get_Fill_Amount;
                        index = i;
                    }
                }
            }

            return m_Player_Transforms[index];
        }
    }

    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Game_Controller = m_Transform.GetComponent<Game_Controller>();
        GameObject data_Holder = GameObject.FindGameObjectWithTag("Load_Game_Data");
        m_Players_In_Game = data_Holder.GetComponent<Game_Scene_Data>().activated_Players;
        //Destroy(data_Holder);
    }

    
    private void Start()
    {
        GameObject[] points = GameObject.FindGameObjectsWithTag("Spawn_Point");
        m_Spawn_Transforms = new Transform[points.Length];
        GameObject[] sliders = GameObject.FindGameObjectsWithTag("Score_Slider");
        m_Score_Sliders = new Score_Slider[sliders.Length];
        m_Slider_Values = new float[sliders.Length];

        for (int i = 0; i < points.Length; i++)
        {
            int index = (int)points[i].GetComponent<Spawn_Point_ID>().spawn_Point;
            m_Spawn_Transforms[index] = points[i].GetComponent<Transform>();
        }

        for (int i = 0; i < sliders.Length; i++)
        {
            int index = (int)sliders[i].GetComponent<Score_Slider>().player_ID_Reference;
            m_Score_Sliders[index] = sliders[i].GetComponent<Score_Slider>();
            m_Score_Sliders[index].set_Slider_Overlay_Color = slider_Overlay_Color[index];
        }

        StartCoroutine(Start_Placement_Itterator());
    }

    
    private void Update()
    {
        if (m_Player_Spawned_In && !m_Game_Over)
        {
            float top_Score = 0.0f;

            Add_Player_Score(out top_Score);

            if (top_Score >= 1.0f)
            {
                m_Game_Controller.On_Win_Event();
                Sort_Win_Player();
            }
        }
    }


    private void Sort_Win_Player()
    {
        float[] scores = new float[4];
        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            //if (m_Player_Transforms[i] != null)
            //    m_Player_Transforms[i].GetComponent<Player_Movement_Controller>().enabled = false;
            scores[i] = m_Score_Sliders[i].get_Fill_Amount;
        }

        Array.Sort(scores, m_Score_Sliders);
        Array.Reverse(m_Score_Sliders);

        GameObject win_Data_Holder = new GameObject();
        win_Data_Holder.tag = "Load_Win_Data";
        Win_Scene_Data data = win_Data_Holder.AddComponent<Win_Scene_Data>();
        data.Initialize();
        data.m_Player_Win_Rank = new Player_ID[m_Active_Player_Amounts];
        
        for (int i = 0; i < m_Active_Player_Amounts; i++)
        {
            data.m_Player_Win_Rank[i] = m_Score_Sliders[i].player_ID_Reference;
        }

        m_Game_Over = true;
    }


    private void Add_Player_Score (out float top_Score)
    {
        top_Score = 0.0f;
        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            if (m_Player_Transforms[i] != null)
            {
                bool in_Water = m_Player_Transforms[i].GetComponent<Player_Movement_Controller>().In_Water;

                if (in_Water)
                {
                    float delta_Value = target_Water_Time / 60.0f;
                    float prev_Value = m_Score_Sliders[i].get_Fill_Amount;
                    m_Score_Sliders[i].On_Value_Change(prev_Value + (delta_Value * Time.deltaTime));
                }
            }
            if (m_Score_Sliders[i].get_Fill_Amount > top_Score)
            {
                top_Score = m_Score_Sliders[i].get_Fill_Amount;
            }
        }
    }


    private void Instantiate_Players ()
    {
        m_Player_Transforms = new Transform[m_Players_In_Game.Length];

        for (int i = 0; i < m_Players_In_Game.Length; i++)
        {
            if (m_Players_In_Game[i])
            {
                m_Player_Transforms[i] = Instantiate(Resources.Load("Player/Player_Prefab", typeof(Transform))) as Transform;
                m_Player_Transforms[i].BroadcastMessage("Initialize_Player_ID", (Player_ID)i);
                m_Player_Transforms[i].BroadcastMessage("Initialize_Player", m_Game_Controller);
                m_Active_Player_Amounts++;
            }
            else
            {
                m_Player_Transforms[i] = null;
            }
        }

        Array.Sort(m_Slider_Values, m_Player_Transforms);
        Array.Reverse(m_Player_Transforms);

        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            if (m_Player_Transforms[i] != null)
            {
                m_Player_Transforms[i].position = m_Spawn_Transforms[i].position;
                m_Player_Transforms[i].rotation = Math_Ex.get_Look_Rotation(m_Player_Transforms[i].position, m_Game_Controller.Track_Origin);
                float offset = m_Player_Transforms[i].GetComponent<Player_Movement_Controller>().Ground_Distance;
                m_Player_Transforms[i].position += m_Player_Transforms[i].up * offset;
            }
        }

        m_Player_Spawned_In = true;
        m_Game_Controller.On_Start_Event();
    }


    private IEnumerator Start_Placement_Itterator()
    {
        bool is_Full = false;

        while (!is_Full)
        {
            float slider_Value = 0.0f;
            for (int i = 0; i < m_Score_Sliders.Length; i++)
            {
                if (m_Players_In_Game[i])
                {
                    float prev_Value = m_Score_Sliders[i].get_Fill_Amount;
                    float value = (get_Interact_Button((Player_ID)i, Button_Event_State.Down)) ? button_Hit_Fill_Amount : 0.0f;
                    m_Score_Sliders[i].On_Value_Change(prev_Value + value);
                    m_Slider_Values[i] = Mathf.Clamp(prev_Value + value, 0.0f, 1.0f);
                    if ((prev_Value + value) > slider_Value)
                        slider_Value = prev_Value + value;
                }
            }
            if (slider_Value >= 1.0f)
                is_Full = true;
            yield return null;
        }

        for (int i = 0; i < m_Score_Sliders.Length; i++)
        {
            m_Score_Sliders[i].On_Value_Change(0.0f);
        }

        Instantiate_Players();
    }
    

    public void On_Object_Pickup(Transform player_Transform, float value)
    {
        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            if (m_Player_Transforms[i] == player_Transform)
            {
                float curr_Value = m_Score_Sliders[i].get_Fill_Amount;
                m_Score_Sliders[i].On_Value_Change(curr_Value + value);
                m_Score_Sliders[i].On_Pickup_Present();
            }
        }
    }


}



